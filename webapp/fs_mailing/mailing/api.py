from django.db import transaction
from rest_framework import viewsets

from mailing.models import Client, Message
from mailing.serializers import ClientSerializer, MessageSerializer
from mailing.views import ApiListViewPagination


class MessageViewSet(viewsets.ReadOnlyModelViewSet):
    '''Получение списка сообщений / сообщения по id'''
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    pagination_class = ApiListViewPagination


class ClientViewSet(viewsets.ModelViewSet):
    '''Получение списка клиентов / клиента по id'''
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    pagination_class = ApiListViewPagination

    @transaction.atomic
    def update(self, request, *args, **kwargs):
        Tag.create_new(request.data.get('tagline', []))
        return super(ClientViewSet, self).update(request, *args, **kwargs)

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        Tag.create_new(request.data.get('tagline', []))
        return super(ClientViewSet, self).create(request, *args, **kwargs)

