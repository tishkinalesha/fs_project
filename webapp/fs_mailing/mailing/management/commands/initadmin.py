from django.contrib.auth.models import UserManager
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):

        user_manager = UserManager()
        user_manager.model = User
        try:
            user = User.objects.get(username='admin')
            print(f"Superuser: '{user.username}' exist!")
        except:
            user = user_manager.create_superuser('admin', 'admin@example.com', 'password')
            print(f"Superuser: '{user.username}' with password: 'password' has been created!")