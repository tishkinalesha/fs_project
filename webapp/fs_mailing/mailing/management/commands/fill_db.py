import random

from faker import Faker
from django.core.management.base import BaseCommand

from mailing.models import Client, Tag, MobileOperator


class Command(BaseCommand):

    def handle(self, *args, **options):
        if Client.objects.all().count() == 0:
            fake = Faker()
            for i in range(1, 1000):
                mobile_operator = '9' + ''.join([str(random.randint(0, 9)) for _ in range(2)])
                phone_number = '7' + mobile_operator + ''.join([str(random.randint(0, 9)) for _ in range(7)])
                mobile_operator, _ = MobileOperator.objects.get_or_create(operator_code=mobile_operator)
                tagline = [fake.city() for _ in range(random.randint(1, 5))]
                for tag in tagline:
                    Tag.objects.get_or_create(tag=tag)
                tagline = Tag.objects.filter(tag__in=tagline)
                tz = fake.timezone()
                Client.objects.create(
                    mobile_operator_code=mobile_operator,
                    phone_number=phone_number,
                    tz=tz
                ).tagline.set(tagline)


