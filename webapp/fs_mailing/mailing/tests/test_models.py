import pytest
from hamcrest import assert_that, equal_to
from django.core.exceptions import ValidationError
from django.db.models import ObjectDoesNotExist
from mailing.models import Client as ClientModel, MobileOperator, Tag


@pytest.mark.django_db
class TestClient:

    invalid_phone_numbers = (pytest.param("89163352060", id="wrong region"),
                             pytest.param("791633520601", id="too long"),
                             pytest.param("7916335206", id="too short"),
                             pytest.param("dgaa521fdsa", id="wrong symbols"))

    @pytest.mark.parametrize("phone_number", invalid_phone_numbers)
    def test_phone_validation(self, phone_number):
        with pytest.raises(ValidationError):
            client = ClientModel(phone_number=phone_number)
            client.full_clean()

    def test_mobile_operator_object_generation(self, operator_code=916):
        with pytest.raises(ObjectDoesNotExist):
            MobileOperator.objects.get(operator_code=operator_code)
        ClientModel.objects.create(phone_number="79163352060")
        mobile_operator = MobileOperator.objects.get(operator_code=operator_code)
        assert_that(operator_code, equal_to(mobile_operator.operator_code))


@pytest.mark.django_db
class TestTag:

    solo_tag = pytest.param(["some_tag"], id="solo_tag")
    tagline = pytest.param(["tag_1", "tag_2", "tag_3"], id="tagline")

    @pytest.mark.parametrize('tagline', (solo_tag, tagline))
    def test_tag_create_new(self, tagline):
        assert_that(Tag.objects.filter(tag__in=tagline).count(), equal_to(0))
        Tag.create_new(tagline)
        assert_that(Tag.objects.filter(tag__in=tagline).count(), equal_to(len(tagline)))




