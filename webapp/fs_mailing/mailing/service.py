import json
import requests


TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NzcyMzQwODcsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IlRBQSJ9.1LpgsZcB7mr1-toU1kbYTKhvd8J7ACabizQRRCu20nI'

HEADERS = {
    'accept': 'application/json',
    'Content-Type': 'application/json'
}


class BearerAuth(requests.auth.AuthBase):

    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers["authorization"] = "Bearer " + self.token
        return r


def send_request(id, phone, text):
    data = {
        'id': id,
        'phone': phone,
        'text': text
    }
    response = requests.post(
        url=f'https://probe.fbrq.cloud/v1/send/{data["id"]}',
        data=json.dumps(data),
        headers=HEADERS,
        auth=BearerAuth(TOKEN)
    )

    return response


