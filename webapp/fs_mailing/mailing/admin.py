from django.contrib import admin
from django.urls import reverse
from django.utils.http import urlencode
from django.utils.html import format_html

from mailing.models import MobileOperator, Tag, Message, Mailing, Client


@admin.register(Mailing)
class MailingAdmin(admin.ModelAdmin):
    list_display = ('id', 'message_text', 'run_date', 'deadline')


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):

    search_fields = ('tag',)
    list_display = ('tag', 'view_clients_link')

    def view_clients_link(self, obj):
        clients = Client.objects.filter(tagline=obj)
        count = clients.count()
        url = (
            reverse("admin:mailing_client_changelist")
            + "?"
            + urlencode({"tagline__tag": f"{obj.tag}"})
        )
        return format_html('<a href="{}">{} Clients</a>', url, count)

    view_clients_link.short_description = "Clients"
    ordering = ['tag',]


@admin.register(MobileOperator)
class MobileOperatorAdmin(admin.ModelAdmin):
    ordering = ['operator_code']


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):

    list_display = ('id', 'mailing', 'phone_number', 'status')
    list_filter = ('status', 'mailing', )
    search_fields = ('client__phone_number',)


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):

    search_fields = ('tagline__tag',)
    list_display = ('id', 'phone_number', 'get_tagline', 'tz')
    list_filter = (('tz', admin.AllValuesFieldListFilter), )

    def get_tagline(self, obj):
        return "\n".join([tag.tag for tag in obj.tagline.all()])
