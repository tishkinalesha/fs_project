from rest_framework import serializers
from timezone_field.rest_framework import TimeZoneSerializerField

from mailing.models import (
    Mailing,
    Client,
    Message,
    MobileOperator,
    Tag,
)


class TagSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tag
        fields = ('tag',)


class MobileOperatorSerializer(serializers.ModelSerializer):

    class Meta:
        model = MobileOperator
        fields = ('operator_code',)


class ClientSerializer(serializers.ModelSerializer):

    tagline = serializers.SlugRelatedField(slug_field='tag',
                                           many=True,
                                           queryset=Tag.objects.all(),
                                           required=False)
    tz = TimeZoneSerializerField(required=False)

    class Meta:
        model = Client
        exclude = ('mobile_operator_code', )


class MailingSerializer(serializers.ModelSerializer):

    mobile_operator_filter = serializers.SlugRelatedField(slug_field='operator_code',
                                                          many=True,
                                                          queryset=MobileOperator.objects.all())
    tag_filter = serializers.SlugRelatedField(slug_field='tag',
                                              many=True,
                                              queryset=Tag.objects.all())

    class Meta:
        model = Mailing
        fields = '__all__'


class MessageSerializer(serializers.ModelSerializer):

    mailing = MailingSerializer()
    client = ClientSerializer()

    class Meta:
        model = Message
        fields = '__all__'


class MailingMessagesSerializer(serializers.ModelSerializer):

    client = serializers.SlugRelatedField(slug_field='phone_number',
                                          read_only=True,
                                          required=False)

    class Meta:
        model = Message
        exclude = ('mailing', 'status')

