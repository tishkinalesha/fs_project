from django.utils import timezone
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save

from mailing.models import Mailing, Client, MobileOperator
from mailing.tasks import mailing_create_task


@receiver(post_save, sender=Mailing)
def post_save_mailing(created, **kwargs):
    mailing = kwargs['instance']
    if created and mailing.deadline > timezone.now():
        mailing_create_task.apply_async((mailing.pk,), eta=mailing.run_date)
    else:
        pass


@receiver(pre_save, sender=Client)
def post_save_client(*args, **kwargs):
    client = kwargs['instance']
    mobile_operator_code, _ = MobileOperator.objects.get_or_create(operator_code=client.phone_number[1:4])
    client.mobile_operator_code = mobile_operator_code

