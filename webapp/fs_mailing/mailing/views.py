from django.db.models import ObjectDoesNotExist
from rest_framework import generics
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response
from rest_framework.views import APIView

from mailing.models import Mailing, Message
from mailing.serializers import ClientSerializer, MailingSerializer, MailingMessagesSerializer


class ApiListViewPagination(LimitOffsetPagination):
    default_limit = 20
    max_limit = 1000


class MailingView(generics.ListAPIView, generics.CreateAPIView):
    '''Получение списка рассылок / Добавление новой рассылки'''
    serializer_class = MailingSerializer
    pagination_class = ApiListViewPagination

    def get_queryset(self):
        mailings = Mailing.objects.all()
        return mailings


class MailingDetailView(generics.RetrieveUpdateDestroyAPIView):
    '''Получение/редиактирование подробной информации о рассылке'''
    serializer_class = MailingSerializer
    queryset = Mailing.objects.filter()


class MailingClientsView(APIView):
    '''Получение клиентов рассылки'''
    pagination_class = ApiListViewPagination

    def get(self, request, pk):
        try:
            mailing = Mailing.objects.get(pk=pk)
            clients = mailing.get_clients()
            clients_data = ClientSerializer(clients, many=True).data
        except ObjectDoesNotExist:
            return Response(status=400)
        else:
            return Response(status=200, data=clients_data)


class MailingMessagesView(APIView):
    '''Получение сообщений рассылки'''

    def get(self, request, pk):
        try:
            mailing = Mailing.objects.get(pk=pk)
            messages = mailing.get_messages()
            data = {
                status: MailingMessagesSerializer(
                    messages.filter(status=status), many=True
                ).data for status in Message.Status.values
            }
        except ObjectDoesNotExist:
            return Response(status=400)
        else:
            return Response(status=200, data=data)


class MailingStatsView(APIView):
    '''Получение статистики по всем рассылкам'''
    pagination_class = ApiListViewPagination

    def get(self, request):
        try:
            mailings = Mailing.objects.all()
            messages_list = []
            for mailing in mailings:
                mailing_data = MailingSerializer(mailing).data
                messages = mailing.get_messages()
                for status in Message.Status.values:
                    mailing_data[status] = messages.filter(status=status).count()
                messages_list.append(mailing_data)
        except ObjectDoesNotExist:
            return Response(status=400)
        else:
            return Response(status=200, data=messages_list)
