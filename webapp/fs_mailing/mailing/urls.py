from django.urls import path
from rest_framework.routers import DefaultRouter
from mailing import views, api
urlpatterns = [
    path('mailing/', views.MailingView.as_view()),
    path('mailing/stats', views.MailingStatsView.as_view()),
    path('mailing/<int:pk>', views.MailingDetailView.as_view()),
    path('mailing/<int:pk>/clients', views.MailingClientsView.as_view()),
    path('mailing/<int:pk>/messages', views.MailingMessagesView.as_view()),
]

router = DefaultRouter()

router.register(r'message', api.MessageViewSet, basename='message')
router.register(r'client', api.ClientViewSet, basename='client')

urlpatterns += router.urls