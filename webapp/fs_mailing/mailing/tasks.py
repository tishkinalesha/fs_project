from django.db.models import ObjectDoesNotExist

from fs_mailing.celery import app
from mailing.models import Message, Mailing
from mailing.service import send_request


@app.task(bind=True)
def send_message_task(self, data):
    try:
        message = Message.objects.get(pk=data['id'])
        if message.status == Message.Status.created.value:
            message.status = Message.Status.sending.value
            message.save()
        response = send_request(**data)
        response.raise_for_status()
        message.status = Message.Status.delivered.value
        message.save()
    except Exception as exc:
        raise self.retry(exc=exc, countdown=60 * 60)


@app.task(bind=True)
def mailing_create_task(self, mailing_id):
    try:
        mailing = Mailing.objects.get(pk=mailing_id)
        clients = mailing.get_clients()
        for client in clients:
            message = Message.objects.create(status=Message.Status.created.value, mailing=mailing, client=client)
            send_message_task.delay(
                {
                    "id": message.id,
                    "phone": client.phone_number,
                    "text": mailing.message_text
                }
            )
    except ObjectDoesNotExist as exc:
        raise self.retry(exc=exc, countdown=10)
    except Exception as exc:
        raise self.retry(exc=exc, countdown=60 * 60)
