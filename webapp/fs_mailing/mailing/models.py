from django.db import models
from django.db.models import Q
from django.conf import settings
from timezone_field import TimeZoneField

from mailing.helpers.helpers import ProjectPhoneNumberField


class MobileOperator(models.Model):
    operator_code = models.PositiveSmallIntegerField('Код мобильного оператора', unique=True)
    # operator_name = models.CharField('Название оператора', max_length=100, default='')

    def __str__(self):
        return f'{self.operator_code}'

    class Meta:
        verbose_name = 'Мобильный оператор'
        verbose_name_plural = 'Мобильные операторы'


class Tag(models.Model):
    tag = models.CharField('Тэг', max_length=50, unique=True)

    def __str__(self):
        return self.tag

    class Meta:
        verbose_name = 'Тэг'
        verbose_name_plural = 'Тэги'
        ordering = ['tag']

    @staticmethod
    def create_new(tagline: list):
        if tagline:
            for tag in tagline:
                result, _ = Tag.objects.get_or_create(tag=tag)



class Mailing(models.Model):
    '''Рассылка:
    - уникальный id рассылки
    - дата и время запуска рассылки
    - текст сообщения для доставки клиенту
    - фильтр свойств клиентов, на которых должна быть произведена рассылка (код мобильного оператора, тег)
    - дата и время окончания рассылки: если по каким-то причинам не успели разослать все сообщения - никакие сообщения
     клиентам после этого времени доставляться не должны'''
    run_date = models.DateTimeField('Дата и время запуска рассылки')
    message_text = models.TextField('Текст сообщения')
    mobile_operator_filter = models.ManyToManyField(MobileOperator, blank=True)
    tag_filter = models.ManyToManyField(Tag, blank=True)
    deadline = models.DateTimeField()

    def __str__(self):
        return f'id: {self.id}, msg: {self.message_text}'

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'

    def get_clients(self):
        query = Q()
        if self.tag_filter.all():
            query.add(Q(tagline__in=self.tag_filter.all()), Q.AND)
        if self.mobile_operator_filter.all():
            query.add(Q(mobile_operator_code__in=self.mobile_operator_filter.all()), Q.AND)
        return Client.objects.filter(query)

    def get_messages(self):
        messages = Message.objects.filter(mailing=self)
        return messages



class Client(models.Model):
    '''Сущность "клиент" имеет атрибуты:
    - уникальный id клиента
    - номер телефона клиента в формате 7XXXXXXXXXX (X - цифра от 0 до 9)
    - код мобильного оператора
    - тег (произвольная метка)
    - часовой пояс'''
    phone_number = ProjectPhoneNumberField('Номер телефона', max_length=11, null=False, blank=False, unique=True)
    tagline = models.ManyToManyField(Tag, verbose_name='Tэг', blank=True)
    mobile_operator_code = models.ForeignKey(MobileOperator, verbose_name='Код мобильного оператора', blank=True,
                                             on_delete=models.SET_NULL, null=True)
    tz = TimeZoneField('Часовой пояс', choices_display="WITH_GMT_OFFSET", default=settings.TIME_ZONE, blank=True)

    def __str__(self):
        return f'id: {self.pk}, phone: {self.phone_number}'

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'

    @property
    def tag_list(self):
        return list(sorted(tag[0] for tag in self.tagline.all().values_list('tag')))


class Message(models.Model):
    '''Сущность "сообщение" имеет атрибуты:
    - уникальный id сообщения
    - дата и время создания (отправки)
    - статус отправки
    - id рассылки, в рамках которой было отправлено сообщение
    - id клиента, которому отправили'''
    class Status(models.Choices):
        created = 'created'
        sending = 'sending'
        delivered = 'delivered'

    create_date = models.DateField('Дата создания', auto_now_add=True)
    status = models.CharField(max_length=20, choices=Status.choices, default=Status.created)
    mailing = models.ForeignKey(Mailing, verbose_name='Рассылка', on_delete=models.SET_NULL, null=True)
    client = models.ForeignKey(Client, verbose_name='Клиент', on_delete=models.SET_NULL, null=True)

    def phone_number(self):
        return self.client.phone_number

    def __str__(self):
        return f'id: {self.id}, ' \
               f'Дата создания: {self.create_date}, ' \
               f'id рассылки: {self.mailing.pk}, ' \
               f'id клиента: {self.client.pk}, ' \
               f'статус: {self.status}'

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
