from django.db.models import CharField
from django.core.validators import RegexValidator
from django.utils.deconstruct import deconstructible
from django.utils.translation import gettext_lazy as _
from django.utils.regex_helper import _lazy_re_compile


@deconstructible
class PhoneValidator(RegexValidator):
    def __init__(self):
        super().__init__()
        self.message = _('Enter a valid phone value like 7XXXXXXXXXX where X is digit from 0 to 9.')
        self.regex = _lazy_re_compile(r'^7\d{10}$')


class ProjectPhoneNumberField(CharField):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.validators.insert(0, PhoneValidator())



