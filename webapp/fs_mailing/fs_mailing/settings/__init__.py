import os

if os.environ.get('POSTGRES_PASSWORD'):
    from fs_mailing.settings.dockerize_settings import *
else:
    from fs_mailing.settings.default_settings import *

