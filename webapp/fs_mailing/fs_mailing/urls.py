from django.contrib import admin
from django.urls import path, include
from fs_mailing.yasg import urlpatterns as documentation_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include('mailing.urls')),
]

urlpatterns += documentation_urls
