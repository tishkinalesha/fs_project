import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'fs_mailing.settings')

app = Celery('fs_mailing')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
